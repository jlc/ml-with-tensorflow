# Machine Learning with TensorFlow

Some exercises using Tensorflow/keras to implement, train and use neural networks.

- `MachineLearning_MNIST_tf2-keras`: contains jupyter notebooks and other files useful to build/train neural networks using tensorflow2 and the MNIST data (hand written digits).
- `API_TOD` : contains some Python scripts useful to implement neural networks training using the Tensorflow Object Dectection API (aka TOD API) -- _this work is still ongoing..._
